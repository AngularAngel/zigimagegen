const Color = @import("main.zig").Color;
const random = @import("std").rand;

const Point = struct {
    x: f32,
    y: f32,
};

pub fn drawToImage(data: []Color, width: u32, height: u32) void {
    var x: u32 = 0;
    while(x < width) : (x += 1) {
        var y: u32 = 0;
        while(y < height) : (y += 1) {
            data[x + y*width] = Color{.r = 0, .g = 0, .b = 0, .a = 255};
        }
    }
    const referencePoints = [_]Point {
        Point{.x = 0, .y = 0},
        Point{.x = 0, .y = @intToFloat(f32, height-1)},
        Point{.x = @intToFloat(f32, width-1), .y = @intToFloat(f32, height-1)},
        Point{.x = @intToFloat(f32, width-1), .y = 0},
        //Point{.x = @intToFloat(f32, width/2), .y = 0},
    };
    const interp: f32 = 0.5;
    var point = Point{.x = 0, .y = 0};
    var isaac64Generator = random.Isaac64.init(0x683975439);
    var rand = random.Isaac64.random(&isaac64Generator);
    var i: u32 = 0;
    var lastIndex: u32 = 0;
    while(i < 200000000) : (i += 1) {
        var nextIndex: u32 = undefined;
        while(true) {
            nextIndex = rand.uintLessThanBiased(u32, referencePoints.len);
            if((nextIndex^2) != lastIndex) break;
        }
        lastIndex = nextIndex;
        var nextPoint = referencePoints[nextIndex];
        point.x = point.x*interp + nextPoint.x*(1 - interp);
        point.y = point.y*interp + nextPoint.y*(1 - interp);
        //@import("std").log.info("Point: {}", .{point});
        if(data[@floatToInt(usize, point.x) + @floatToInt(usize, point.y)*width].r < 255) {
            data[@floatToInt(usize, point.x) + @floatToInt(usize, point.y)*width].r += 1;
            data[@floatToInt(usize, point.x) + @floatToInt(usize, point.y)*width].g += 1;
            data[@floatToInt(usize, point.x) + @floatToInt(usize, point.y)*width].b += 1;
        }
    }
}