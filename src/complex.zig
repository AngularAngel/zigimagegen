pub const Complex = struct {
    r: f32,
    i: f32,

    pub fn add(this: Complex, other: Complex) Complex {
        return Complex{
            .r = this.r + other.r,
            .i = this.i + other.i
        };
    }
    // (real₁ + img₁*i)*(real₂ + img₂*i) = real₁*real₂ + img₁*i*real₂ + real₁*img₂*i + img₁*i*img₂*i
    // real₁*real₂ - img₁*img₂ + (img₁*real₂ + real₁*img₂)*i
    pub fn mul(this: Complex, other: Complex) Complex {
        return Complex{
            .r = this.r*other.r - this.i*other.i,
            .i = this.r*other.i + this.i*other.r,
        };
    }

    pub fn valueSquare(this: Complex) f32 {
        return this.r*this.r + this.i*this.i;
    }
};