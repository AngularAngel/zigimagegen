const Color = @import("main.zig").Color;
const std = @import("std");
const random = std.rand;
const pow = std.math.pow;

pub fn toImage(data: []Color, comptime width: u32, comptime height: u32) void {
    var noise: [width*height]f32 = undefined;
    nearThreshold(noise[0..], width, height, 0x7658376);
    var y: u32 = 0;
    while(y < height) : (y += 1) {
        var x: u32 = 0;
        while(x < width) : (x += 1) {
            data[x + y*width].r = @floatToInt(u8, 128 + noise[x + y*width]*255);
            data[x + y*width].g = @floatToInt(u8, 128 + noise[x + y*width]*255);
            data[x + y*width].b = @floatToInt(u8, 128 + noise[x + y*width]*255);
            data[x + y*width].a = 255;
        }
    }
}

pub fn s(in: f32) f32 {
    return (3 - 2*in)*in*in;
}

pub fn nearThreshold(data: []f32, comptime width: u32, comptime height: u32, seed: u64) void {
    _ = data;
    _ = width;
    _ = height;
    _ = seed;
    var isaac64Generator = random.Isaac64.init(seed);
    var rand = random.Isaac64.random(&isaac64Generator);
    _ = rand;

    const allocator = std.heap.page_allocator;
    var y: u32 = 0;
    var x: u32 = 0;
    y = 0;
    while(y < height) : (y += 1) {
        x = 0;
        while(x < width) : (x += 1) {
            data[x + y*width] = 0;
        }
    }

    const initialResolution: u32 = 32;
    var resolution : u32 = initialResolution;
    while(resolution != 0) : (resolution /= 2) {
        var startingData = allocator.alloc(f32, (width/resolution + 2)*(height/resolution + 2)) catch unreachable;
        defer allocator.free(startingData);
        _ = startingData;

        y = 0;
        while(y < height/resolution + 2) : (y += 1) {
            x = 0;
            while(x < width/resolution + 2) : (x += 1) {
                startingData[x + y*(width/resolution + 2)] = (rand.float(f32) - 0.5) * 2;
            }
        }

        y = 0;
        while(y < height) : (y += 1) {
            x = 0;
            while(x < width) : (x += 1) {
                var sum: f32 = 0;
                var x0: u32 = x/resolution;
                var y0: u32 = y/resolution;
                var weightX: f32 = s(@intToFloat(f32, x - x0*resolution)/@intToFloat(f32, resolution));
                var weightY: f32 = s(@intToFloat(f32, y - y0*resolution)/@intToFloat(f32, resolution));
                var dy: u32 = 0;
                while(dy < 2) : (dy += 1) {
                    var dx: u32 = 0;
                    while(dx < 2) : (dx += 1) {
                        var weight: f32 = 1;
                        if(dx == 0) {
                            weight *= 1 - weightX;
                        } else {
                            weight *= weightX;
                        }
                        if(dy == 0) {
                            weight *= 1 - weightY;
                        } else {
                            weight *= weightY;
                        }
                        sum += weight*startingData[(y0 + dy)*(width/resolution + 2) + (x0 + dx)];
                    }
                }

                data[x + y*width] += sum*@intToFloat(f32, resolution)/@intToFloat(f32, initialResolution)/2;
            }
        }
    }
}

pub fn nearThreshold2(data: []f32, comptime width: u32, comptime height: u32, seed: u64) void {
    _ = data;
    _ = width;
    _ = height;
    _ = seed;
    var isaac64Generator = random.Isaac64.init(seed);
    var rand = random.Isaac64.random(&isaac64Generator);
    _ = rand;

    const allocator = std.heap.page_allocator;
    const blurSize : u32 = 14;
    var startingData = allocator.alloc(f32, (width + blurSize)*(height + blurSize)) catch unreachable;
    defer allocator.free(startingData);
    _ = startingData;

    var y: u32 = 0;
    var x: u32 = 0;
    while(y < height + blurSize) : (y += 1) {
        x = 0;
        while(x < width + blurSize) : (x += 1) {
            startingData[x + y*(width + blurSize)] = rand.float(f32);
        }
    }

    y = 0;
    while(y < height) : (y += 1) {
        x = 0;
        while(x < width) : (x += 1) {
            var sum: f32 = 0;
            var dy: u32 = 0;
            while(dy <= blurSize) : (dy += 1) {
                var dx: u32 = 0;
                while(dx <= blurSize) : (dx += 1) {
                    sum += startingData[(y + dy)*(width + blurSize) + (x + dx)];
                }
            }

            data[x + y*width] = sum/@intToFloat(f32, (blurSize+1)*(blurSize+1));
        }
    }
}