const Color = @import("main.zig").Color;
const Complex = @import("complex.zig").Complex;

// zₙ₊₁ = zₙ² + c

fn mandelbrot(x: f32, y: f32) u32 {
    const c = Complex{.r = x, .i = y};
    var currentZ = c;
    var index: u32 = 0;
    while(index < 1000 and currentZ.valueSquare() < 16) {
        currentZ = currentZ.mul(currentZ).add(c);
        index += 1;
    }
    return index;
}

pub fn drawToImage(data: []Color, width: u32, height: u32) void {
    var x: u32 = 0;
    while(x < width) : (x += 1) {
        var y: u32 = 0;
        while(y < height) : (y += 1) {
            var iterations: u32 = mandelbrot(
                @intToFloat(f32, x)*2.5/@intToFloat(f32, width) - 2,
                @intToFloat(f32, y)*2/@intToFloat(f32, height) - 1,
            );
            data[x + y*width] = Color {
                .r = @intCast(u8, iterations%256),
                .g = 0,
                .b = 0,
                .a = 255,
            };
        }
    }
}