const Color = @import("main.zig").Color;
const std = @import("std");
const random = std.rand;
const pow = std.math.pow;
const Order = std.math.Order;
const PriorityQueue = std.PriorityQueue;
const noise = @import("noise.zig");

var isaac64Generator: random.Isaac64 = undefined;
var rand: random.Random = undefined;

const Normal = struct {
    fn spreadFunction(this: Normal, source: SpreadPoint, dX: i32, dY: i32) f32 {
        _ = this;
        _ = source;
        _ = dX;
        _ = dY;
        return 0;
    }
};

const Cardinal = struct {
    fn spreadFunction(this: Cardinal, source: SpreadPoint, dX: i32, dY: i32) f32 {
        _ = this;
        _ = source;
        if (dX != 0)
            return -0.25 + @intToFloat(f32, std.math.absInt(source.y - source.origin.y) catch 0);
        if (dY != 0)
            return -0.25 + @intToFloat(f32, std.math.absInt(source.x - source.origin.x) catch 0);
        unreachable;
    }
};

const Directional = struct {
    x: f32,
    y: f32,
    width: f32,
    fn init() Directional {
        return Directional {
            .x = 2*rand.floatNorm(f32) - 1,
            .y = 2*rand.floatNorm(f32) - 1,
            .width = 2 + 50*rand.float(f32),
        };
    }
    fn spreadFunction(this: Directional, source: SpreadPoint, dX: i32, dY: i32) f32 {
        _ = this;
        _ = source;
        _ = dX;
        _ = dY;
        var dx: f32 = @intToFloat(f32, source.x - source.origin.x);
        var dy: f32 = @intToFloat(f32, source.y - source.origin.y);
        var directionality = this.x*dx + this.y*dy;
        directionality = @maximum(directionality, -directionality);
        if(directionality > this.width) {
            return (directionality - this.width)*0.1;
        } else {
            return -0.2;
        }
    }
};

const Circle = struct {
    radiusSquare: f32,
    fn init() Circle {
        return Circle {
            .radiusSquare = 10 + 5000*rand.float(f32),
        };
    }
    fn spreadFunction(this: Circle, source: SpreadPoint, dX: i32, dY: i32) f32 {
        _ = this;
        _ = source;
        _ = dX;
        _ = dY;
        var dx: f32 = @intToFloat(f32, source.x - source.origin.x);
        var dy: f32 = @intToFloat(f32, source.y - source.origin.y);
        if(dx*dx + dy*dy > this.radiusSquare) {
            return 1000;
        } else {
            return -0.25;
        }
    }
};

const Griddo = struct {
    gridSize: i32,
    fn init() Griddo {
        return Griddo {
            .gridSize = rand.intRangeAtMost(i32, 20, 250),
        };
    }
    fn spreadFunction(this: Griddo, source: SpreadPoint, dX: i32, dY: i32) f32 {
        _ = this;
        _ = source;
        if (dX != 0)
            return -(0.1 + @intToFloat(f32, this.gridSize)) * 0.0015 + @intToFloat(f32, @mod(std.math.absInt(source.y - source.origin.y) catch 0, this.gridSize)) * 50;
        if (dY != 0)
            return -(0.1 + @intToFloat(f32, this.gridSize)) * 0.0015 + @intToFloat(f32, @mod(std.math.absInt(source.x - source.origin.x) catch 0, this.gridSize)) * 50;
        unreachable;
    }
};

const RectoGriddo = struct {
    gridSizeX: i32,
    gridSizeY: i32,
    fn init() RectoGriddo {
        return RectoGriddo {
            .gridSizeX = rand.intRangeAtMost(i32, 10, 100),
            .gridSizeY = rand.intRangeAtMost(i32, 10, 100),
        };
    }
    fn spreadFunction(this: RectoGriddo, source: SpreadPoint, dX: i32, dY: i32) f32 {
        _ = this;
        _ = source;
        if (dX != 0)
            return -(0.1 + @intToFloat(f32, this.gridSizeX)) * 0.0015 + @intToFloat(f32, @mod(std.math.absInt(source.y - source.origin.y) catch 0, this.gridSizeX)) * 50;
        if (dY != 0)
            return -(0.1 + @intToFloat(f32, this.gridSizeY)) * 0.0015 + @intToFloat(f32, @mod(std.math.absInt(source.x - source.origin.x) catch 0, this.gridSizeY)) * 50;
        unreachable;
    }
};

const River = struct {
    x: f32,
    y: f32,
    width: f32,
    freq: f32,
    amp: f32,
    fn init() River {
        var width = 10 + 30*rand.float(f32);
        return River {
            .x = 2*rand.floatNorm(f32) - 1,
            .y = 2*rand.floatNorm(f32) - 1,
            .width = width,
            .freq = 1/width/2/3.1415926535897,
            .amp = 2*width,
        };
    }
    fn spreadFunction(this: River, source: SpreadPoint, dX: i32, dY: i32) f32 {
        _ = this;
        _ = source;
        _ = dX;
        _ = dY;
        var dx: f32 = @intToFloat(f32, source.x - source.origin.x);
        var dy: f32 = @intToFloat(f32, source.y - source.origin.y);
        var directionality = this.x*dx + this.y*dy;
        var otherDirectionality = this.x*dy - this.y*dx;
        directionality -= @sin(otherDirectionality*this.freq)*this.amp;
        directionality = @maximum(directionality, -directionality);
        if(directionality > this.width) {
            return (directionality - this.width)*0.1;
        } else {
            return -0.4;
        }
    }
};

const PointTypes = enum {
    normal,
    cardinal,
    directional,
    circle,
    griddo,
    rectoGriddo,
    river,
};

const Spreader = union(PointTypes) {
    normal: Normal,
    cardinal: Cardinal,
    directional: Directional,
    circle: Circle,
    griddo: Griddo,
    rectoGriddo: RectoGriddo,
    river: River,
    fn init(enumValue: PointTypes) Spreader {
        switch(enumValue) {
            .normal => return Spreader{.normal = Normal{}},
            .cardinal => return Spreader{.cardinal = Cardinal{}},
            .directional => return Spreader{.directional = Directional.init()},
            .circle => return Spreader{.circle = Circle.init()},
            .griddo => return Spreader{.griddo = Griddo.init()},
            .rectoGriddo => return Spreader{.rectoGriddo = RectoGriddo.init()},
            .river => return Spreader{.river = River.init()},
        }
    }
    fn spreadFunction(this: Spreader, source: SpreadPoint, dX: i32, dY: i32) f32 {
        switch(this) {
            .normal => return this.normal.spreadFunction(source, dX, dY),
            .cardinal => return this.cardinal.spreadFunction(source, dX, dY),
            .directional => return this.directional.spreadFunction(source, dX, dY),
            .circle => return this.circle.spreadFunction(source, dX, dY),
            .griddo => return this.griddo.spreadFunction(source, dX, dY),
            .rectoGriddo => return this.rectoGriddo.spreadFunction(source, dX, dY),
            .river => return this.river.spreadFunction(source, dX, dY),
        }
    }
};


const VoronoiPoint = struct {
    x: i32,
    y: i32,
    strength: f32,
    color: Color,
    spreader: Spreader,

    //fn distanceSqr(self: @This(), x: f32, y: f32) f32 {
    //    return pow(f32, self.x - x, 2) + pow(f32, self.y - y, 2);
    //}
};


const SpreadPoint = struct {
    x: i32,
    y: i32,
    curStrength: f32,
    origin: *VoronoiPoint,

    fn addNeighbor(this: SpreadPoint, dX: i32, dY: i32, strDecr: f32, comptime width: u32, comptime height: u32, spreadQueue: *PriorityQueue(SpreadPoint, void, compareFn)) void {
        var new = SpreadPoint {
            .x = this.x + dX,
            .y = this.y + dY,
            .curStrength = @minimum(this.curStrength, this.curStrength - strDecr - this.origin.spreader.spreadFunction(this, dX, dY)),
            .origin = this.origin,
        };
        if(new.x < 0 or new.y < 0 or new.x >= width or new.y >= height) {
            return;
        }
        spreadQueue.add(new) catch unreachable;
    }
};

fn compareFn(context: void, a: SpreadPoint, b: SpreadPoint) Order {
    _ = context;
    if(a.curStrength < b.curStrength) {
        return Order.gt;
    }
    if(a.curStrength > b.curStrength) {
        return Order.lt;
    }
    return Order.eq;
}

const spacing : i32 = 25;
const strengthMult : i32 = 130;

pub fn jitteredGrid(data: []Color, comptime width: u32, comptime height: u32) void {
    isaac64Generator = random.Isaac64.init(0x68739);
    rand = random.Isaac64.random(&isaac64Generator);
    const allocator = std.heap.page_allocator;
    var spreadQueue = PriorityQueue(SpreadPoint, void, compareFn).init(allocator, {});
    defer spreadQueue.deinit();

    var points : [width/spacing][height/spacing]VoronoiPoint = undefined;
    var i: u31 = 0;
    while(i < points.len) : (i += 1) {
        var j: u31 = 0;
        while(j < points[0].len) : (j += 1) {
            points[i][j].x = spacing * i + rand.intRangeAtMost(i32, 0, spacing);
            points[i][j].y = spacing * j + rand.intRangeAtMost(i32, 0, spacing);
            points[i][j].strength = @intToFloat(f32, strengthMult * spacing) * rand.float(f32);
            points[i][j].color.a = 255;
            points[i][j].color.r = rand.int(u8);
            points[i][j].color.g = rand.int(u8);
            points[i][j].color.b = rand.int(u8);
            points[i][j].spreader = Spreader.init(rand.enumValue(PointTypes));
            spreadQueue.add(SpreadPoint{
                .x = points[i][j].x,
                .y = points[i][j].y,
                .curStrength = points[i][j].strength,
                .origin = &points[i][j],
            }) catch unreachable;
        }
    }
    var x: u32 = 0;
    while(x < width) : (x += 1) {
        var y: u32 = 0;
        while(y < height) : (y += 1) {
            data[x + y*width].a = 0;
        }
    }

    var xSpreadBias: [width*height]f32 = undefined;
    var ySpreadBias: [width*height]f32 = undefined;
    noise.nearThreshold(xSpreadBias[0..], width, height, 0x754248967);
    noise.nearThreshold(ySpreadBias[0..], width, height, 0x76589389);

    while(spreadQueue.count() > 0) {
        var current = spreadQueue.remove();
        const index: usize = @intCast(usize, current.x + current.y*width);
        var curPixel = &data[index];
        if(curPixel.a == 0) {
            curPixel.* = current.origin.color;
            var strDecr = rand.float(f32);
            var xDecr = xSpreadBias[index];
            var yDecr = ySpreadBias[index];
            
            current.addNeighbor(0, -1, strDecr - yDecr, width, height, &spreadQueue);
            current.addNeighbor(1, 0, strDecr + xDecr, width, height, &spreadQueue);
            current.addNeighbor(0, 1, strDecr + yDecr, width, height, &spreadQueue);
            current.addNeighbor(-1, 0, strDecr - xDecr, width, height, &spreadQueue);
        }
    }
}