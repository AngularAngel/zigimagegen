const Color = @import("main.zig").Color;
const random = @import("std").rand;
const pow = @import("std").math.pow;
const noise = @import("noise.zig");

const VoronoiPoint = struct {
    x: f32,
    y: f32,
    strength: f32,
    color: Color,

    fn distanceSqr(self: @This(), x: f32, y: f32) f32 {
        return pow(f32, self.x - x, 2) + pow(f32, self.y - y, 2);
    }
};

const len : u32 = 256;
const threshold : f32 = pow(f32, 150, 2);

pub fn nearThreshold(data: []Color, width: u32, height: u32) void {
    var isaac64Generator = random.Isaac64.init(0x683975439);
    var rand = random.Isaac64.random(&isaac64Generator);
    var points = [_]VoronoiPoint {undefined} ** len;
    var i: u32 = 0;
    while(i < len) : (i += 1) {
        while(true) {
            points[i].x = rand.float(f32) * @intToFloat(f32, width);
            points[i].y = rand.float(f32) * @intToFloat(f32, height);
            var closestDistanceSqr: f32 = 1e50;
            var j: u32 = 0;
            while(j < i) : (j += 1) {
                const distSqr = points[j].distanceSqr(points[i].x, points[i].y);
                if(distSqr < closestDistanceSqr) {
                    closestDistanceSqr = distSqr;
                }
            }
            if(closestDistanceSqr < threshold or i == 0) {
                break;
            }
        }
        points[i].color.a = 255;
        points[i].color.r = rand.int(u8);
        points[i].color.g = rand.int(u8);
        points[i].color.b = rand.int(u8);
    }
    drawToImage(points[0..], data, width, height);
}

const spacing : u32 = 25;
const strengthMult : i32 = 100;

pub fn jitteredGrid(data: []Color, comptime width: u32, comptime height: u32) void {
    var isaac64Generator = random.Isaac64.init(0x6786639);
    var rand = random.Isaac64.random(&isaac64Generator);
    var points : [width/spacing][height/spacing]VoronoiPoint = undefined;
    var i: u32 = 0;
    while(i < points.len) : (i += 1) {
        var j: u32 = 0;
        while(j < points[0].len) : (j += 1) {
            points[i][j].x = @intToFloat(f32, spacing * i) + rand.float(f32) * @intToFloat(f32, spacing);
            points[i][j].y = @intToFloat(f32, spacing * j) + rand.float(f32) * @intToFloat(f32, spacing);
            points[i][j].strength = @intToFloat(f32, strengthMult * spacing) * rand.float(f32);
            points[i][j].color.a = 255;
            points[i][j].color.r = rand.int(u8);
            points[i][j].color.g = rand.int(u8);
            points[i][j].color.b = rand.int(u8);
        }
    }

    i = 0;
    while(i < points.len) : (i += 1) {
        var j: u32 = 0;
        while(j < points[0].len) : (j += 1) {
            var closest : VoronoiPoint = undefined;
            var closestDistance: f32 = 1e50;
            var centerX: i32 = @intCast(i32, i);
            var centerY: i32 = @intCast(i32, j);
            var gridX: i32 = -strengthMult;
            const localPoint : VoronoiPoint = points[i][j];
            while(gridX < strengthMult + 1) : (gridX += 1) {
                var gridY: i32 = -strengthMult;
                while(gridY < strengthMult + 1) : (gridY += 1) {
                    var pointX = centerX + gridX;
                    var pointY = centerY + gridY;
                    if(pointX < 0 or pointX >= points.len or pointY < 0 or pointY >= points[0].len) {
                        continue;
                    }
                    const point : VoronoiPoint = points[@intCast(u32, pointX)][@intCast(u32, pointY)];
                    const dist = @sqrt(point.distanceSqr(localPoint.x, localPoint.y)) + localPoint.strength - point.strength;
                    if(dist < closestDistance) {
                        closestDistance = dist;
                        closest = point;
                    }
                }
            }
            points[i][j] = closest;
        }
    }

    var x: u32 = 0;
    while(x < width) : (x += 1) {
        var y: u32 = 0;
        while(y < height) : (y += 1) {
            var closest : VoronoiPoint = undefined;
            var closestDistance: f32 = 1e50;
            var centerX: i32 = @intCast(i32, x/spacing);
            var centerY: i32 = @intCast(i32, y/spacing);
            var gridX: i32 = -2;
            while(gridX < 3) : (gridX += 1) {
                var gridY: i32 = -2;
                while(gridY < 3) : (gridY += 1) {
                    var pointX = centerX + gridX;
                    var pointY = centerY + gridY;
                    if(pointX < 0 or pointX >= points.len or pointY < 0 or pointY >= points[0].len) {
                        continue;
                    }
                    const point : VoronoiPoint = points[@intCast(u32, pointX)][@intCast(u32, pointY)];
                    const dist = @sqrt(point.distanceSqr(@intToFloat(f32, x), @intToFloat(f32, y))) - point.strength;
                    if(dist < closestDistance) {
                        closestDistance = dist;
                        closest = point;
                    }
                }
            }
            data[x + y*width] = closest.color;
        }
    }
}

pub fn curvyEdgeJitteredGrid(data: []Color, comptime width: u32, comptime height: u32) void {
    var isaac64Generator = random.Isaac64.init(0x6786639);
    var rand = random.Isaac64.random(&isaac64Generator);
    var points : [width/spacing][height/spacing]VoronoiPoint = undefined;
    var i: u32 = 0;
    while(i < points.len) : (i += 1) {
        var j: u32 = 0;
        while(j < points[0].len) : (j += 1) {
            points[i][j].x = @intToFloat(f32, spacing * i) + rand.float(f32) * @intToFloat(f32, spacing);
            points[i][j].y = @intToFloat(f32, spacing * j) + rand.float(f32) * @intToFloat(f32, spacing);
            points[i][j].strength = @intToFloat(f32, strengthMult * spacing); //* rand.float(f32);
            points[i][j].color.a = 255;
            points[i][j].color.r = rand.int(u8);
            points[i][j].color.g = rand.int(u8);
            points[i][j].color.b = rand.int(u8);
        }
    }

    i = 0;
    while(i < points.len) : (i += 1) {
        var j: u32 = 0;
        while(j < points[0].len) : (j += 1) {
            var closest : VoronoiPoint = undefined;
            var closestDistance: f32 = 1e50;
            var centerX: i32 = @intCast(i32, i);
            var centerY: i32 = @intCast(i32, j);
            var gridX: i32 = -strengthMult;
            const localPoint : VoronoiPoint = points[i][j];
            while(gridX < strengthMult + 1) : (gridX += 1) {
                var gridY: i32 = -strengthMult;
                while(gridY < strengthMult + 1) : (gridY += 1) {
                    var pointX = centerX + gridX;
                    var pointY = centerY + gridY;
                    if(pointX < 0 or pointX >= points.len or pointY < 0 or pointY >= points[0].len) {
                        continue;
                    }
                    const point : VoronoiPoint = points[@intCast(u32, pointX)][@intCast(u32, pointY)];
                    const dist = @sqrt(point.distanceSqr(localPoint.x, localPoint.y)) + localPoint.strength - point.strength;
                    if(dist < closestDistance) {
                        closestDistance = dist;
                        closest = point;
                    }
                }
            }
            points[i][j] = closest;
        }
    }

    var xDisplacement: [width*height]f32 = undefined;
    var yDisplacement: [width*height]f32 = undefined;
    noise.nearThreshold(xDisplacement[0..], width, height, 0x75489367);
    noise.nearThreshold(yDisplacement[0..], width, height, 0x76589367589);

    var x: u32 = 0;
    while(x < width) : (x += 1) {
        var y: u32 = 0;
        while(y < height) : (y += 1) {
            var closest : VoronoiPoint = undefined;
            var closestDistance: f32 = 1e50;
            var displacedX = xDisplacement[x + y*width]*100 + @intToFloat(f32, x);
            var displacedY = yDisplacement[x + y*width]*100 + @intToFloat(f32, y);
            var centerX: i32 = @floatToInt(i32, displacedX/@intToFloat(f32, spacing));
            var centerY: i32 = @floatToInt(i32, displacedY/@intToFloat(f32, spacing));
            var gridX: i32 = -2;
            while(gridX < 3) : (gridX += 1) {
                var gridY: i32 = -2;
                while(gridY < 3) : (gridY += 1) {
                    var pointX = centerX + gridX;
                    var pointY = centerY + gridY;
                    if(pointX < 0 or pointX >= points.len or pointY < 0 or pointY >= points[0].len) {
                        continue;
                    }
                    const point : VoronoiPoint = points[@intCast(u32, pointX)][@intCast(u32, pointY)];
                    const dist = @sqrt(point.distanceSqr(displacedX, displacedY)) - point.strength;
                    if(dist < closestDistance) {
                        closestDistance = dist;
                        closest = point;
                    }
                }
            }
            data[x + y*width] = closest.color;
        }
    }
}

pub fn drawToImage(points: []VoronoiPoint, data: []Color, width: u32, height: u32) void {
    var x: u32 = 0;
    while(x < width) : (x += 1) {
        var y: u32 = 0;
        while(y < height) : (y += 1) {
            var closest : VoronoiPoint = points[0];
            var closestDistanceSqr: f32 = closest.distanceSqr(@intToFloat(f32, x), @intToFloat(f32, y));
            var i: u32 = 1;
            while(i < points.len) : (i += 1) {
                const distSqr = points[i].distanceSqr(@intToFloat(f32, x), @intToFloat(f32, y));
                if(distSqr < closestDistanceSqr) {
                    closestDistanceSqr = distSqr;
                    closest = points[i];
                }
            }
            data[x + y*width] = closest.color;
        }
    }
}