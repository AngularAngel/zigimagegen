const std = @import("std");
const mandelbrot = @import("mandelbrot.zig");
const chaosGame = @import("chaosGame.zig");
const voronoi = @import("voronoi.zig");
const noise = @import("noise.zig");
const floodfill = @import("floodfill.zig");
const c = @cImport({
    @cInclude("stb_image_write.h");
});

pub const Color = packed struct {
    r: u8,
    g: u8,
    b: u8,
    a: u8,
};

const width: usize = 1920;
const height: usize = 1080;
const channels: usize = 4;

pub fn main() anyerror!void {
    // Note that info level log messages are by default printed only in Debug
    // and ReleaseSafe build modes.
    const allocator = std.heap.page_allocator;
    var data = try allocator.alloc(Color, width*height);
    defer allocator.free(data);
    //noise.toImage(data, width, height);
    floodfill.jitteredGrid(data, width, height);
    //voronoi.curvyEdgeJitteredGrid(data, width, height);

    _ = c.stbi_write_png("test.png", width, height, channels, data.ptr, width * channels);
}

test "basic test" {
    try std.testing.expectEqual(10, 3 + 7);
}